Title: 2024 April Fool's Puzzle and Competition Rankings
Date: 2024-04-17 12:00
Slug: 2024-april-fools-rankings
Author: yano

Thank you for everybody who participated! Congratulations to those who finished!

## Solo

|  Ranking |  NickServ Account | Finish Time (UTC)   |
|----------|-------------------|---------------------|
| 1.       | qt                | 2024-04-06 13:08:40 |
| 2.       | switchnode        | 2024-04-06 18:53:58 |
| 3.       | ali1234           | 2024-04-06 21:31:53 |
| 4.       | artart78          | 2024-04-06 21:55:57 |
| 5.       | pinkieval         | 2024-04-07 21:25:20 |
| 6.       | Nyaaori           | 2024-04-07 21:35:21 |
| 7.       | nikuhodai         | 2024-04-07 22:46:00 |
| 8.       | ncf               | 2024-04-09 10:25:08 |
| 9.       | BlueShark         | 2024-04-09 22:13:00 |
| 10.      | dfch              | 2024-04-09 22:34:09 |

<br />

## Teams

|  Ranking |  NickServ Account | Finish Time (UTC)   |
|----------|-------------------|---------------------|
| 1.       | shikhin           | 2024-04-06 17:10:43 |
| 1.       | zgrep             | 2024-04-07 14:16:06 |

<br />

For 2024, we started the competition at 12:00 UTC on 2024-04-01.
<br />
Updated: 2024-04-17 by 'yano'
